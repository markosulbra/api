package br.com.boletos.application;

public class Messages {
    public static final String MESSAGE_SUCCESS = "Solicitação realizada com sucesso!";
    public static final String MESSAGE_FAIL = "Ocorreu um erro ao tentar completar a sua requisição, tente novamente mais tarde!";
    public static final String MESSAGE_NOTREGISTER = "Nenhum registro encontrado!";

}
